<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductAction
{
    const POPULAR_COUNT = 1;

    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function execute(): GetMostPopularProductResponse
    {
        $products = $this->repository->findAll();
        usort($products, function(Product $currentProduct, Product $nextProduct){
            return $currentProduct->getRating() < $nextProduct->getRating();
        });
        $product = array_slice($products, 0, self::POPULAR_COUNT);
        return new GetMostPopularProductResponse($product[0]);
    }
}