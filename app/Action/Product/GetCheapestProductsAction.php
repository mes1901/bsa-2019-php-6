<?php

declare(strict_types=1);

namespace App\Action\Product;

use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetCheapestProductsAction
{
    const CHEAPEST_COUNT = 3;

    private $repository;

    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
    
    public function execute(): GetCheapestProductsResponse
    {
        $products = $this->repository->findAll();
        usort($products, function(Product $currentProduct, Product $nextProduct){
            return $currentProduct->getPrice() > $nextProduct->getPrice();
        });
        return new GetCheapestProductsResponse(array_slice($products, 0, self::CHEAPEST_COUNT));
    }
}