<?php

declare(strict_types=1);

namespace App\Action\Product;


use App\Entity\Product;
use App\Repository\ProductRepositoryInterface;

class GetMostPopularProductResponse
{
    private $popularProducts;

    public function __construct(Product $popularProducts)
    {
        $this->popularProducts = $popularProducts;
    }

    public function getProduct(): Product
    {
        return $this->popularProducts;
    }
}