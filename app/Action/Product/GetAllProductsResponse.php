<?php

declare(strict_types=1);

namespace App\Action\Product;


class GetAllProductsResponse
{
    private $allProducts;

    public function __construct(array $allProducts)
    {
        $this->allProducts = $allProducts;
    }

    public function getProducts(): array
    {
        return $this->allProducts;
    }
}