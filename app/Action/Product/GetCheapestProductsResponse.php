<?php

declare(strict_types=1);

namespace App\Action\Product;


class GetCheapestProductsResponse
{

    private $cheapestProducts;

    public function __construct(array $cheapestProducts)
    {
        $this->cheapestProducts = $cheapestProducts;
    }

    public function getProducts(): array
    {
        return $this->cheapestProducts;
    }
}