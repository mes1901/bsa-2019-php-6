<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Product;

class ProductGenerator
{
    /**
     * @return Product[]
     */
    public static function generate(): array
    {
        return [
            new Product(1, 'Video Card',6000,"https://i1.rozetka.ua/goods/2027639/copy_msi_rx_580_armor_4g_592587591b1ce_images_2027639700.jpg", 5),
            new Product(2, 'CPU',4000,"https://i1.rozetka.ua/goods/2238131/intel_core_i3_8100_images_2238131231.jpg", 4.5),
            new Product(3, 'Motherboard',7000,"https://i1.rozetka.ua/goods/7894570/asus_rog_strix_z390_e_gaming_images_7894570143.jpg", 4.1),
            new Product(4, 'RAM',3000,"https://i1.rozetka.ua/goods/3791893/kingston_hx432c18fb2k2_16_images_3791893400.jpg", 4.9),
            new Product(5, 'HDD',2000,"https://i1.rozetka.ua/goods/7968725/57347133_images_7968725985.jpg", 3.9),
            new Product(6, 'PC Case',1500,"https://i1.rozetka.ua/goods/10574048/1st_player_v6_r1_color_led_images_10574048354.jpg", 4),
            new Product(7, 'Power Supply',2500,"https://i1.rozetka.ua/goods/11423172/enermax_erb700awt_tr_images_11423172660.jpg", 4.4),
            new Product(8, 'Monitor',5000,"https://i1.rozetka.ua/goods/10482431/dell_210_apwu_images_10482431708.jpg", 4.6),
            new Product(9, 'Optical drive',1000,"https://i2.rozetka.ua/goods/11998131/85239900_images_11998131774.jpg", 3.8),
            new Product(10, 'Router',500,"https://i2.rozetka.ua/goods/10932230/tp_link_tl_wr841n_images_10932230727.jpg", 4.7)
        ];
    }
}