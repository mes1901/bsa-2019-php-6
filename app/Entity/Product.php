<?php

declare(strict_types=1);

namespace App\Entity;

class Product
{
    private $id;
    private $name;
    private $price;
    private $imageUrl;
    private $rating;

    public function __construct(int $id, string $name, float $price, string $imageUrl, float $rating)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->imageUrl = $imageUrl;
        $this->rating = $rating;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getImageUrl(): string
    {
        return $this->imageUrl;
    }

    public function getRating(): float
    {
        return $this->rating;
    }

}