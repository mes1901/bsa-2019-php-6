<?php

namespace App\Http\Controllers;

use App\Action\Product\GetAllProductsAction;
use App\Action\Product\GetCheapestProductsAction;
use App\Action\Product\GetMostPopularProductAction;
use App\Http\Presenter\ProductArrayPresenter;
use App\Repository\ProductRepositoryInterface;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAllProducts()
    {
        $products = app(GetAllProductsAction::class);
        $jsonProduct = ProductArrayPresenter::presentCollection($products->execute()->getProducts());
        return response()->json($jsonProduct);
    }

    public function getPopularProduct()
    {
        $product = app(GetMostPopularProductAction::class);        
        $jsonProduct = ProductArrayPresenter::present($product->execute()->getProduct());
        return response()->json($jsonProduct);
    }

    public function getCheapestProducts()
    {
        $products = app(GetCheapestProductsAction::class);
        $cheapestProducts = $products->execute()->getProducts();
        return view('cheap_products', compact('cheapestProducts'));

    }


}