<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Product;

class ProductArrayPresenter
{
    /**
     * @param Product[] $products
     * @return array
     */
    public static function presentCollection(array $products): array
    {
        $prodCollection = [];
        foreach ($products as $product) {
            array_push($prodCollection, [
                'id' => $product->getId(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'img' => $product->getImageUrl(),
                'rating' => $product->getRating()
            ]);
        }
        return $prodCollection;
    }

    public static function present(Product $product): array
    {
        return [
            'id' => $product->getId(),
            'name' => $product->getName(),
            'price' => $product->getPrice(),
            'img' => $product->getImageUrl(),
            'rating' => $product->getRating()
        ];
    }
}
