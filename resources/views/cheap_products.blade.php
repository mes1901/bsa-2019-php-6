<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Cheap products</title>
</head>
<body>
    <h1>List of cheap products</h1>
        <ul>
            @foreach ($cheapestProducts as $product)
                <li><img src="{{ $product->getImageUrl() }}" />{{ $product->getName() }} - {{ $product->getPrice() }}</li>
            @endforeach
        </ul>
</body>
</html>